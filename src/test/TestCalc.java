import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import static jdk.nashorn.internal.objects.Global.Infinity;


public class TestCalc {

    private Calc calc;

    @BeforeClass
    public void runBeforeEveryTest()
    {
        System.out.println("Start testing");
    }
    @BeforeTest
    public void  initTest()
    {
        calc = new Calc();
    }
    @Test
    public void getAdd() throws Exception
    {
        double res = calc.methodCalc("2", "+", "3");
        Assert.assertEquals(5.0, res);
        System.out.println("Test of addition -> OK");
    }
    @Test
    public void getSub() throws Exception
    {
        double res = calc.methodCalc("10", "-", "7.5");
        Assert.assertEquals(2.5, res);
        System.out.println("Test of subtraction -> OK");
    }
    @Test
    public void getMul() throws  Exception
    {
        double res = calc.methodCalc("4.2", "*", "2");
        Assert.assertEquals(8.4, res);
        System.out.println("Test of multiplication -> OK");
    }
    @Test
    public void getDiv() throws Exception
    {
        double res = calc.methodCalc("57", "/", "12");
        Assert.assertEquals(4.75, res);
        System.out.println("Test of division -> OK");
    }
    @Test
    public void getDivByZero() throws Exception
    {
        double res = calc.methodCalc("25", "/", "0");
        Assert.assertEquals(Infinity, res);
        System.out.println("Test of division by zero -> OK");
    }
    @Test
    public void getWrongOpMark() throws  Exception
    {
        try {
            calc.methodCalc("4","r","5");
            Assert.assertTrue(false);
        } catch (Exception e) {
            Assert.assertTrue(true);
        }
        System.out.println("Test of wrong math mark -> OK");
    }
    @Test
    public void getNoDigital() throws Exception
    {
        try {
            calc.methodCalc("f", "*", "3");
            Assert.assertTrue(false);
        } catch (Exception e) {
            Assert.assertTrue(true);
        }
        System.out.println("Test of entering not digital -> OK");
    }

    @AfterClass
    public  static void afterClass()
    {
        System.out.println("Finish testing");
    }
}
