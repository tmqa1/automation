import org.testng.Assert;
import org.testng.annotations.*;

import java.util.Iterator;
import java.util.List;

/**
 * Created by cosmofish on 20.07.17.
 */
public class TestList {

    @BeforeSuite
    public void beforeSuite(){
        System.out.println("<<<<<<<<<<<<<<<<<<< Before Suite >>>>>>>>>>>>>>>>>>>>>>>>");
    }
    @BeforeClass
    public void BeforeEveryClass(){
        System.out.println("=================== Before Class ===================");
    }

    @BeforeTest
    public void  beforeTest(){
        System.out.println("******************* Before Test *******************");
    }

    @BeforeMethod
    public void beforeMethod(){
        System.out.println("Test case stated");
    }

    @Test
    void sizeTestCase(){
        List<Object>list = new MyArrayList();
        Assert.assertTrue(list.size()==0);

    }

    @Test
    void isEmptyTestCase(){
        MyArrayList list = new MyArrayList();
        Assert.assertTrue(list.isEmpty());

    }

    @Test
    void addTestCase(){
        MyArrayList list = new MyArrayList();
        list.add("5");
        list.add("3");
        Assert.assertEquals(list.size(), 2, "Incorrect list size");
    }
    @Test
    void addMoreThanTenTest(){
        MyArrayList list = new MyArrayList();
        for (int i =0; i <11; i++) {
            list.add("test");
        }
        Assert.assertEquals(list.size(), 11, "Incorrect list size");
    }

    @Test
    void clearTestCase(){
        MyArrayList list = new MyArrayList();
        list.clear();
        Assert.assertTrue(list.size()==0);
    }

    @Test
    void removeTestCase(){
        MyArrayList list = new MyArrayList();
        list.add("test");
        list.add("test1");
        list.add("test2");
        Assert.assertTrue(list.remove("test1"), "there is no such element");
        Assert.assertEquals(list.size(), 2,"Incorrect list size");
    }

    @Test
    void  addAllTestCase(){
    MyArrayList myArrayList = new MyArrayList();
    MyArrayList myArrayList1 = new MyArrayList();
    myArrayList.add("raz");
    myArrayList.add("dva");
    myArrayList1.add("tri");
    myArrayList1.add("fore");
    myArrayList1.addAll(myArrayList);
    Assert.assertEquals(4,myArrayList1.size(), "collections not merged");
    for (Object obj : myArrayList1.toArray()) {
        System.out.println(obj);
    }
    }

    @Test
    void iteratorTestCase(){
        MyArrayList myArrayList = new MyArrayList();
        myArrayList.add("raz");
        myArrayList.add("dva");
        Iterator iterator = myArrayList.iterator();
        while (iterator.hasNext()){
            System.out.println(iterator.next());
        }
        System.out.println("Test iterator true");
    }

    @AfterMethod
    public void afterMethod(){
        System.out.println("Test case finished");
    }
    @AfterTest
    public void  afterTest(){
        System.out.println("******************* After Test ********************");
    }

    @AfterClass
    public  static void afterClass()
    {
        System.out.println("=================== After Class ===================");
    }

    @AfterSuite
    public void afterSuite(){
        System.out.println("<<<<<<<<<<<<<<<<<<< After Suite >>>>>>>>>>>>>>>>>>>>>>>>");
    }

}
