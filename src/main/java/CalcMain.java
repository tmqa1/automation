import java.util.InputMismatchException;
import java.util.Scanner;

/**
 * Created by cosmofish on 04.07.17.
 */
public class CalcMain {


    public  static  void  main (String[] args)
    {
        try {
            Scanner sc = new Scanner(System.in);
            System.out.println("Enter first number");
            String n1 = sc.next();
            System.out.println("Enter operation mark");
            String op = sc.next();
            System.out.println("Enter second number");
            String n2 = sc.next();
            sc.close();

            System.out.println("Result:" + Calc.methodCalc(n1, op, n2));
        } catch (InputMismatchException e) {
            System.out.println(e.getMessage());
        } catch (Exception e) {
            System.out.println("Enter digital!!!");
        }
    }
}
