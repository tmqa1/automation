import java.lang.reflect.Array;
import java.util.*;

/**
 * Created by cosmofish on 18.07.17.
 */
public class MyArrayList implements List {

    int capacity = 10;
    Object[] array = new Object[capacity];
    int size;

    public MyArrayList() {
        array = new Object[capacity];
    }

    public int size() {
        return size;
    }

    @Override
    public boolean isEmpty() {
        return size == 0;
    }

    @Override
    public boolean contains(Object obj) {
        if (obj == null) return false;

        for (int i = 0; i < size; i++) {
            if (obj!=null && array[i].equals(obj))
                return true;
        }
        return false;

    }


    @Override
    public Iterator iterator() {
        return new  MyIterator();
    }

    @Override
    public Object[] toArray() {
        return new Object[0];
    }

    @Override
    public Object[] toArray(Object[] a) {
        return new Object[0];
    }

    @Override
    public boolean add (Object obj) {
        if (size < capacity){
            array[size] = obj;
        } else {
            capacity +=10;
            Object[] arrBig = new Object[capacity];
            for (int i = 0; i < size; i++){
                arrBig[i] = array;
            }
            arrBig[size+1] = obj;
            array = arrBig;
        }
      size++;
        return true;
    }

    @Override
    public boolean remove(Object obj) {
        if (obj==null) return false;
        for (int i = 0; i < size; i++) {
            if (array[i].equals(obj)) {
                for (int k = i; k < size; k++){
                    array[k] = array[k + 1];
                }
                size--;
                return true;
            }
        }
        return false;
    }


    @Override
    public boolean addAll(Collection c) {
        MyIterator iterator = (MyIterator) c.iterator();
        while (iterator.hasNext()){
            add(iterator.next());
        }
        return true;
    }

    @Override
    public boolean addAll(int index, Collection c) {

        return false;
    }

    @Override
    public boolean retainAll(Collection c) {
        return false;
    }

    @Override
    public boolean removeAll(Collection c) {
        return false;
    }

    @Override
    public boolean containsAll(Collection c) {
        return false;
    }

    @Override
    public void clear() {
        Object[] newArray = new Object[capacity];
        array = newArray;
        size = 0;

    }

    @Override
    public Object get(int index) {
        if(index<=size){return array[index];
        }else{
            System.out.println("Such index not found");
            return false;
        }

    }

    @Override
    public Object set(int index, Object element) {
        if (index < 0 || index >= size)
            throw new ArrayIndexOutOfBoundsException();
        Object oldVal = array[index];
        array[index] = element;
        return oldVal;

    }

    @Override
    public void add(int index, Object element) {

    }

    @Override
    public Object remove(int index) {
        return null;
    }

    @Override
    public int indexOf(Object o) {
        if(size!=0){
            for(int i=0;i<size;i++){
                if(array[i].equals(o)){
                    return i;
                }
            }
        }
        return -1;

    }

    @Override
    public int lastIndexOf(Object o) {
        return 0;
    }

    @Override
    public ListIterator listIterator() {
        return null;
    }

    @Override
    public ListIterator listIterator(int index) {
        return null;
    }

    @Override
    public List subList(int fromIndex, int toIndex) {
        return null;
    }

    private class MyIterator implements Iterator{

        private int index;
        @Override
        public boolean hasNext() {
            return index != size;
        }

        @Override
        public Object next() {
            return Array.get(array, index++);
        }

        @Override
        public void remove() {
            System.out.println("Realization is not appropriate");

        }
    }
}
