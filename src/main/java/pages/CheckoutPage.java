package pages;

import lombok.extern.log4j.Log4j;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import services.CustomService;
import services.User;

import java.util.List;

import static services.CustomService.*;

/**
 * Created by cosmofish on 05.09.17.
 */
@Log4j
public class CheckoutPage {
    final WebDriver driver;
    public CheckoutPage(WebDriver driver){
        this.driver = driver;
    }

    @FindBy(xpath=".//*[contains(@id,'form-email')]")
    public WebElement emailField;

    @FindBy(xpath = ".//*[contains(@id,'form-fullname')]")
    public WebElement fullName;

    @FindBy(xpath = ".//*[contains(@id,'form_countryiso2_chosen')]")
    public WebElement country;

    @FindBy(xpath = ".//*[contains(@id,'form_countryiso2_chosen')]//input")
    public WebElement countryInput;

    @FindBy(xpath = ".//*[contains(@id,'form_countryiso2_chosen')]//li")
    public WebElement countryChosen;

    @FindBy(xpath = "//*[contains(@id,'form-phone')][@type='text']")
    public WebElement phone;

    @FindBy(xpath = "//*[contains(@id,'form-postalcode')]")
    public WebElement postCode;

    @FindBy(xpath = ".//*[contains(@id,'form_stateiso2_chosen')]")
    public WebElement state;

    @FindBy(xpath = ".//*[contains(@id,'form_stateiso2_chosen')]//input")
    public WebElement stateInput;

    @FindBy(xpath = ".//*[contains(@id,'form_stateiso2_chosen')]//li")
    public WebElement stateChosen;

    @FindBy(xpath = ".//*[contains(@id,'form-cityname')]")
    public WebElement city;

    @FindBy(xpath = ".//*[contains(@id,'billinginfo') and contains(@id,'form-submit')]")
    public WebElement paymentButton;

    @FindBy(xpath = "//*[contains(@id,'form_phone_code_chosen')]/a")
    public WebElement phoneCode;

    @FindBy(xpath=".//*[contains(@id,'form_phone_code_chosen')]/div/div/input")
    public WebElement phoneCodeInput;

    @FindBy(xpath=".//*[contains(@id,'form_phone_code_chosen')]/div/ul/li")
    public WebElement phoneCodeChosen;

    @FindBy(xpath = "//*[contains(@id,'checkout-payment-buy')]")
    public List<WebElement> paymentTypesList;

    public enum Panel {signin, information, payment}

    public void submitFullName(User user){
        CustomService.waitForElementClickable(fullName, driver);
        sendKeys(fullName, "Full Name field", user.getFullName());
        log.info("Inserted full name");
    }


    public void submitEmail (User user){
        waitForElementClickable(emailField, driver);
        sendKeys(emailField, "Email field", user.getEmail());
        emailField.sendKeys(Keys.ENTER);
        log.info("Inserted email");

    }

    public void submitCountry(User user){
        clickOnElement(country, "country", driver);
        waitForElementClickable(country, driver);
        sendKeys(country, "Country field", user.getCountry());
        countryChosen.sendKeys(Keys.ENTER);
        log.info("Inserted country");
    }
    public void submitState(User user){
        clickOnElement(state, "state", driver);
        waitForElementClickable(state, driver);
        sendKeys(stateInput, "State field", user.getState());
        stateChosen.sendKeys(Keys.ENTER);
        log.info("Inserted state");
    }
    public void submitPhone(User user){
        waitForElementClickable(phone, driver);
        sendKeys(phone, "Insert phone field", user.getPhone());
        log.info("Insert phone");

    }
    public void submitPostCode(User user){
        waitForElementClickable(postCode, driver);
        sendKeys(postCode, "Postal Code field", user.getCountryCode());
        log.info("Insert Post Code");
    }
    public void submitCity(User user){
        waitForElementClickable(city, driver);
        sendKeys(city, "City field", user.getCity());
        log.info("Insert City");
    }

    public void clickPaymentButton(){
        clickOnElement(paymentButton, "Continue Payment", driver);
        log.info("Click continue payment button");
    }


    public void fillingForm(User user){
        submitEmail(user);
        submitFullName(user);
        submitPhone(user);
        submitCity(user);
        submitPostCode(user);
        if (user.getCountry().equals("United States"))
            submitState(user);
        clickPaymentButton();
    }
    public void payBy(PaymentMethods paymentMethod){
        waitForElementClickable(paymentTypesList.get(0), driver);
        WebElement payment = driver.findElement(By.id("checkout-payment-buy-"+paymentMethod));

        clickOnElement(payment, paymentMethod + " button", driver);
        log.info("Pay by "+paymentMethod);
    }


}
