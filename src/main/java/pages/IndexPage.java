package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import services.CustomService;
import services.User;

import static org.openqa.selenium.support.PageFactory.initElements;
import static services.CustomService.clickOnElement;

/**
 * Created by cosmofish on 30.08.17.
 */
public class IndexPage {

    final WebDriver driver;

    public IndexPage(WebDriver driver) {
        this.driver = driver;
    }

    @FindBy(id = "header-signin-link")
    public WebElement accountLink;

    @FindBy(id = "user-avatar")
    public WebElement userAvatar;

    @FindBy(id = "header-signOut-link")
    public WebElement signOutLink;

    @FindBy(id = "menu-favorites")
    public WebElement favorites;


    public void clickAccountLink(){
        clickOnElement(accountLink, "Account link", driver);
    }

    public void logout(){
        clickOnElement(userAvatar, "Account menu", driver);
        clickOnElement(signOutLink, "Sign out link", driver);
    }

    public void logInFromIndexPage(User user){
        clickAccountLink();
        CustomService.switchToLastWindow(driver);
        AuthPage authPage = initElements(driver, AuthPage.class);
        authPage.loginByAcc(user);
    }

    public void loginByFaceBook(User user){
        clickAccountLink();
        CustomService.switchToLastWindow(driver);
        AuthPage authPage = initElements(driver, AuthPage.class);
        authPage.loginByFb(user);
    }


    }
