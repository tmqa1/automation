package pages;

import lombok.extern.log4j.Log4j;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.List;
import java.util.Random;

import static services.CustomService.*;

/**
 * Created by cosmofish on 04.09.17.
 */
@Log4j
public class CartPage {

    final WebDriver driver;
    Random random = new Random();

    public CartPage(WebDriver driver) {
        this.driver = driver;
    }

    @FindBy(xpath = "//div[contains(@class,'active')]//button[@data-toggle='modal']")
    public WebElement addToCartButton;

    @FindBy(id = "purchase_options")
    public WebElement purchaseOptions;

    @FindBy(id = "lc-board-close")
    public WebElement chatCloseBtn;

    @FindBy(xpath = ".//button[contains(@class,'checkout-button')]")
    public WebElement checkoutNowButton;

    @FindBy(xpath = "//*[contains(@class,'recommended')]//strong[contains(@class,'service-name')]")
    public List<WebElement> onTemplateOffersNamesList;

    @FindBy(xpath = "//*[contains(@class,'recommended')]//*[contains(@id,'add-offer')]")
    public List<WebElement> onTemplateOffersButtonsList;

    @FindBy(xpath = "//*[contains(@class,'upsells')]//strong[contains(@class,'service-name')]")
    public List<WebElement> onCartOffersNamesList;

    @FindBy(xpath = "//*[contains(@class,'upsells')]//*[contains(@id,'add-offer')]")
    public List<WebElement> onCartOffersButtonsList;


    public void clickAddToCartButton() {
        //Click "Add To Cart" button.
        clickOnElement(addToCartButton, "Add To Cart Button", driver);

    }

    private void clickOnRandomOffer(List<WebElement> buttons) {
        int numberOnCard = random.nextInt(buttons.size());
        waitForElementClickable(buttons.get(numberOnCard), driver);
        clickOnElement(buttons.get(numberOnCard), "Random on card offer", driver);

        log.info("Select random cart offer");
        waitJqueryComplete(driver, 15);
    }


    public void clickAddToCartButton(String license) {
        List<WebElement> options = purchaseOptions.findElements(By.xpath("//a[contains(@class,'panel-title')]/h4"));
        Integer index = null;
        //Select necessary license by text.
        for (WebElement option : options) {
            if (option.getText().equals(license)) {
                clickOnElement(option, license, driver);
                index = options.indexOf(option);

            }
        }
        //Click "Add to Cart" button.
        clickOnElement(driver.findElement(By.xpath(".//*[@id='collapse" + index + "']//*[@data-target='#cart-popup']")),
                "Add To Cart Button.", driver);
    }

    public enum OfferTypes {
        OnCart,
        OnTemplate
    }


    public void selectOffer(OfferTypes offerType) {
        waitJqueryComplete(driver, 15);
        switch (offerType) {
            case OnCart:
                if (onCartOffersButtonsList.isEmpty()) {
                    log.error("On Cart offers is absent");
                    break;
                }
                clickOnRandomOffer(onCartOffersButtonsList);
                waitForElementClickable(checkoutNowButton, driver);
                break;

            case OnTemplate:
                if (onTemplateOffersButtonsList.isEmpty()) {
                    log.error("On Template offers is absent");
                    break;
                }
                clickOnRandomOffer(onTemplateOffersButtonsList);
                waitForElementClickable(checkoutNowButton, driver);
                break;

            default:
                throw new IllegalArgumentException("Incorrect offer");
        }
    }


    public void clickCheckoutNowButton(){

        String thirdCheckout = "checkout.php";

        if (elementIsDisplayed(chatCloseBtn, "chatCloseBtn")) {
            clickOnElement(chatCloseBtn, "chatCloseBtn", driver);
        }

        //Click "Checkout Now" button.
        clickOnElement(checkoutNowButton, "Checkout Now Button", driver);
        //Wait Checkout page.
    }



}
