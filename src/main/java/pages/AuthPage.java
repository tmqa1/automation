package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import services.User;

import static services.CustomService.*;

/**
 * Created by cosmofish on 30.08.17.
 */
public class AuthPage {

    final WebDriver driver;

    public AuthPage(WebDriver driver) {
        this.driver = driver;
    }

    @FindBy(xpath = "//input[@type='email']")
    public WebElement emailField;

    @FindBy(xpath = "//*[contains(@id,'facebook')]//button")
    public WebElement facebookButton;

    @FindBy(xpath = "//input[@type='password']")
    public WebElement passwordField;

    @FindBy(xpath = "//*[contains(@id,'continue')]//button")
    public WebElement continueButton;

    @FindBy(xpath = "//*[contains(@id,'login')]//button")
    public WebElement logInButton;

    @FindBy(className = "app-logo")
    public WebElement tmLogo;

    @FindBy(id = "email")
    public WebElement fbEmail;

    @FindBy(id = "pass")
    public WebElement fbPass;

    @FindBy(xpath = "//*[@type='submit']")
    public WebElement fbSubmit;

    @FindBy(xpath = "//button[contains(@name, 'CONFIRM')]")
    public WebElement fbConfirmButton;


    public void loginByAcc(User user){

        waitForElementClickable(emailField, driver);
        sendKeys(emailField, "Email field", user.getEmail());
        clickOnElement(continueButton, "Continue button", driver);

        waitForElementClickable(passwordField, driver);
        sendKeys(passwordField, "Password field", user.getPassword());
        clickOnElement(logInButton, "Continue button", driver);
    }

    public void loginByFb(User user){

        waitForElementClickable(facebookButton, driver);
        clickOnElement(facebookButton, "FB button", driver);
        switchToLastWindow(driver);
        waitForElementClickable(fbEmail, driver);
        sendKeys(fbEmail, "Email field", user.getEmail());
        sendKeys(fbPass,"Pass field", user.getPassword());
        clickOnElement(fbSubmit, "Submit button", driver);
        waitForElementClickable(fbConfirmButton, driver);
        clickOnElement(fbConfirmButton, "Confirm button", driver);

    }





}
