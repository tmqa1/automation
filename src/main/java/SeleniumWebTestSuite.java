import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.Test;

import java.util.List;

/**
 * Created by cosmofish on 15.08.17.
 */
public class SeleniumWebTestSuite{

    @Test
    void test(){

        WebDriver driver = new FirefoxDriver();

        WebElement favorite = driver.findElement(By.xpath(".//li[contains(@id,'signin')]/preceding-sibling::li//span[@id='menu-favorites']"));

        List<WebElement> types = driver.findElements(By.xpath(".//*[@class='sub-menu container']//*[contains(@class, 'js-sub-menu-1')]/ul/li"));

        WebElement fashion = driver.findElement(By.id("categories-fashions"));

        WebElement third = driver.findElement(By.xpath(".//li[@data-slide-id='0']//li[@class='thumbnail'][3]//b[contains(@id,'favorite')]"));

    }
}
