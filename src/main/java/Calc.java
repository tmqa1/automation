/**
 * Created by cosmofish on 04.07.17.
 */
public class Calc {
    public static double methodCalc (String s1, String op, String s2) throws Exception
    {
        double a1 = Double.parseDouble(s1);

        double a2 = Double.parseDouble(s2);
        double res;

        switch (op)
        {
            case "+": res= a1 + a2;
                break;
            case "-": res = a1 - a2;
                break;
            case "*": res = a1 * a2;
                break;
            case "/": res = a1 / a2;
                break;
            default:

                throw new Exception("Wrong operation mark!");
        }
        return res;
    }
}
