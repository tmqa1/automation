package smoke;
import org.testng.Assert;
import org.testng.annotations.Test;
import pages.IndexPage;
import services.Constants;
import services.User;

import static org.openqa.selenium.support.PageFactory.initElements;
import static org.testng.Assert.assertEquals;
import static services.CustomService.CookiesService.getCookieValue;
import static services.CustomService.getURL;
import static services.CustomService.waitFavoriteLoad;

/**
 * Created by cosmofish on 31.08.17.
 */
public class LoginByFB extends BaseTest {
    User user = new User("properties/fbUser.properties");

    @Test
    public void loginByFB(){
        IndexPage indexPage = initElements(driver, IndexPage.class);

        //go to index page
        getURL(Constants.URL, driver);
        waitFavoriteLoad(driver);

        //login and go back to index page
        indexPage.loginByFaceBook(user);

        //check that  user avatar is visible
        Assert.assertTrue(indexPage.userAvatar.isDisplayed(), "User avatar is not visible");

        //check that lgn cookie present
        assertEquals((getCookieValue("lgn", driver)), user.getEmail(), "Incorrect cookie lgn value");

    }
}
