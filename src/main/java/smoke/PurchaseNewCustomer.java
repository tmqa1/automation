package smoke;

import lombok.extern.log4j.Log4j;
import org.testng.annotations.Test;
import pages.CartPage;
import pages.CheckoutPage;
import pages.IndexPage;
import services.Constants;
import services.User;

import static org.openqa.selenium.support.PageFactory.initElements;
import static org.testng.Assert.assertTrue;
import static pages.CartPage.OfferTypes.OnCart;
import static pages.CartPage.OfferTypes.OnTemplate;
import static services.CustomService.*;

/**
 * Created by cosmofish on 04.09.17.
 */
@Log4j
public class PurchaseNewCustomer extends BaseTest {

    @Test
    public void test() throws InterruptedException {

        User user = new User("properties/user.properties");

        IndexPage indexPage = initElements(driver, IndexPage.class);

        //go to template page
        getURL(Constants.TEMPLATE_URL, driver);

        //Click Added to Cart button.
        CartPage cartPage = initElements(driver, CartPage.class);
        cartPage.clickAddToCartButton();

        //select on cart offer
        cartPage.selectOffer(OnCart);

        //select on template offer
        cartPage.selectOffer(OnTemplate);

        //Click "Checkout Now" button
        cartPage.clickCheckoutNowButton();

        //Registration
        CheckoutPage checkoutPage = initElements(driver, CheckoutPage.class);
        checkoutPage.fillingForm(user);

        //Pay By Stripe
        checkoutPage.payBy(PaymentMethods.PayPal);
        waitPageLoader("PayPal", 30, driver);

        //Check that we on right page
        assertTrue(getCurrentURL().contains("PayPal"), "Incorrect url");
    }
}
