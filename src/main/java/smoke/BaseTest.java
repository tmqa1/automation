package smoke;

import lombok.extern.log4j.Log4j;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.Map;

/**
 * Created by cosmofish on 29.08.17.
 */
@Log4j
public class BaseTest {
    public WebDriver driver;

    static Map<String, String> allText;

    @BeforeTest
    void init () throws MalformedURLException {
        DesiredCapabilities capabilities = DesiredCapabilities.chrome();
        driver = new RemoteWebDriver(new URL("http://localhost:4444/wd/hub"), capabilities);
        driver.manage().window().maximize();
        log.info("Maximize window");
    }

    @AfterTest
    public void closeBrowser() {
        driver.quit();
    }


}
