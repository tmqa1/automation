package services;

import lombok.Data;

import java.util.Objects;

/**
 * Created by cosmofish on 30.08.17.
 */

@Data
public class User {

    private String userName;
    private String password;
    private String errorMessage;
    private String email;
    private String country;
    private String fullName;
    private String state;
    private String phone;
    private String post;
    private String city;
    private String formAddress;
    private String countryCode;
    private String stateCode;
    private String question;
    private String answer;


    public User(String fileLocation) {
        PropertyReader propertyReader = new PropertyReader(fileLocation);
        this.userName = propertyReader.getValue("userName");
        this.password = propertyReader.getValue("password");
        this.errorMessage = propertyReader.getValue("errorMessage");
        this.fullName = propertyReader.getValue("fullName");
        this.formAddress = propertyReader.getValue("formAddress");
        this.city = propertyReader.getValue("city");
        this.country = propertyReader.getValue("country");
        this.countryCode = propertyReader.getValue("countryCode");
        this.state = propertyReader.getValue("state");
        this.stateCode = propertyReader.getValue("stateCode");
        this.post = propertyReader.getValue("post");
        this.phone = propertyReader.getValue("phone");
        this.question = propertyReader.getValue("question");
        this.answer = propertyReader.getValue("answer");

        //Generate random email.
        String emailRandom = propertyReader.getValue("email");
        if (emailRandom != null) {
            this.email = emailRandom.substring(0, emailRandom.indexOf("@")) + "-" + String.valueOf(System.currentTimeMillis()) + emailRandom.substring(emailRandom.indexOf("@"));
        }
    }
    public void refreshEmail() {
        email = email.replaceAll("\\d+", Objects.toString(System.currentTimeMillis()));
    }

}
