package services;

import lombok.extern.log4j.Log4j;
import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import pages.IndexPage;

import java.util.Iterator;
import java.util.concurrent.TimeUnit;

import static java.lang.Thread.sleep;
import static org.openqa.selenium.support.PageFactory.initElements;
import static org.testng.Assert.assertTrue;

/**
 * Created by cosmofish on 30.08.17.
 */
@Log4j
public class CustomService {
    private static WebDriver driver;


    public CustomService(WebDriver driver){
        this.driver=driver;
    }

    public  static String getCurrentURL(){
        log.info("Current URL:"+ driver.getCurrentUrl());
        return driver.getCurrentUrl();
    }

    public static void getURL(String url) {
        driver.getCurrentUrl();
        log.info("Navigate to \""+url+"\".");
        try {
            driver.manage().timeouts().pageLoadTimeout(60, TimeUnit.SECONDS);
            driver.get(url);

            log.info("Navigate to \""+url+"\" finished.");
            moveToCoordinate(0,0);
        }
        catch (TimeoutException e){
            stopLoad(driver);
        }
    }

    public static void moveToCoordinate(int x, int y) {

        Actions actions = new Actions(driver);
        actions.moveByOffset(x, y).build().perform();
        log.info("Move to coordinate "+x+"x"+y);
    }




    public static void waitForElementClickable(WebElement element, WebDriver driver) {

        try {
            WebDriverWait wait = new WebDriverWait(driver,30);
            wait.until(ExpectedConditions.elementToBeClickable(element));
        }
        catch (TimeoutException e){
            assertTrue(false, "ELEMENT: \"" + element + "\" is not clickable.");
        }
        catch (StaleElementReferenceException e){
            log.error("ELEMENT: \"" + element + "\" is not found in the cache.");
        }
    }


    public void waitUrl(String url){
        WebDriverWait wait = new WebDriverWait(driver, 15);
        wait.until(ExpectedConditions.urlContains(url));
    }

    public static void sendKeys(WebElement element, String elementName, String inputText){

        try {
            element.sendKeys(inputText);
            log.error("\"" + elementName + "\" input text: \"" + inputText + "\".");
        }
        catch (NoSuchElementException e){
            assertTrue(false, "\"" + elementName + "\" was not found on page after timeout.");
        }
        catch (ElementNotVisibleException e){
            assertTrue(false, "\"" + elementName + "\" was not visible.");
        }
    }


    public static void getURL(String url, WebDriver driver) {
        driver.getCurrentUrl();
        log.info("Navigate to \""+url+"\".");
        try {
            driver.manage().timeouts().pageLoadTimeout(60, TimeUnit.SECONDS);
            driver.get(url);

            log.info("Navigate to \""+url+"\" finished.");

        }
        catch (TimeoutException e){
            stopLoad(driver);
        }
    }


    public void waitElement(String xpathElement){
        WebDriverWait wait = new WebDriverWait(driver, 15);
        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(xpathElement)));
    }

    public static void switchToLastWindowClose(WebDriver driver){
        try {
            WebDriverWait wait = new WebDriverWait(driver, 5);
            wait.until(new ExpectedCondition<Boolean>() {
                @Override
                public Boolean apply(WebDriver driver) {
                    return (driver.getWindowHandles().size() > 1) ;
                }
            });
            String last = "";
            Iterator<String> iterator = driver.getWindowHandles().iterator();
            while (iterator.hasNext()){
                last=iterator.next();
            }
            for (String win:driver.getWindowHandles()){
                if (win.equals(last)) {
                    driver.switchTo().window(win);
                }
                else driver.close();
            }
            log.info("Switch to another window.");
        }
        catch (TimeoutException e){
            assertTrue(false, "You have only one window.");
        }
        driver.manage().window().maximize();
        log.info("Maximize window.");
    }


    public static void clickOnElement(WebElement element, String elementName, WebDriver driver){

        try {
            WebDriverWait wait = new WebDriverWait(driver,20);
            wait.until(ExpectedConditions.elementToBeClickable(element));}
        catch (TimeoutException ex){
            log.info("\"" + elementName + "\" is not clickable.");
            log.info("Click on \"" + elementName + "\".");
            return;
        }
        try {
            element.click();
            log.info("Click on \"" +elementName+"\".");
        }
        catch (NoSuchElementException e){
            assertTrue(false, "\"" + elementName + "\" was not found on page after timeout.");
        }
        catch (ElementNotVisibleException e){
            log.error("ElementNotVisibleException");
        }
        catch (TimeoutException e){
            log.error(driver);
        }
        catch (StaleElementReferenceException e){
            log.error("StaleElementReferenceException.");
            log.info("Click on \"" +elementName+"\".");
            element.click();
        }
        catch (WebDriverException e){
            log.error("WebDriverException" +e);
        }
    }

    public static void stopLoad(WebDriver driver){
        driver.findElement(By.tagName("body")).sendKeys(Keys.ESCAPE);
        log.info("Timeout on loading page \""+driver.getCurrentUrl()+"\".");
    }

    public static class CookiesService {

        public static String getCookieValue(String cookieName, WebDriver driver) {
            if (driver.manage().getCookieNamed(cookieName) != null) {
                String value = driver.manage().getCookieNamed(cookieName).getValue();
                log.info("Cookie: \"" + cookieName + "\" has value - \"" + value + "\".");
                return value;
            } else {
                assertTrue(false, "Cookie: \"" + cookieName + "\" was not found after timeout.");
                return null;
            }

        }
    }

    public static void waitJqueryComplete(WebDriver driver,long timeout){
        WebDriverWait wait = new WebDriverWait(driver, timeout);
        try {
            wait.until((WebDriver webDriver) -> ((JavascriptExecutor)driver).executeScript("return !!window.jQuery && window.jQuery.active == 0"));
        } catch (TimeoutException e) {
            log.info("No one jQuery activity or activity continues");
        }
    }

    public static void switchToLastWindow(WebDriver driver){
        try {
            WebDriverWait wait = new WebDriverWait(driver, 5);
            wait.until(new ExpectedCondition<Boolean>() {
                @Override
                public Boolean apply(WebDriver driver) {
                    return (driver.getWindowHandles().size() > 1) ;
                }
            });
            for (String win:driver.getWindowHandles()){
                driver.switchTo().window(win);
            }
            log.info("Switch to another window.");
        }
        catch (TimeoutException e){
            assertTrue(false, "You have only one window.");
        }
        driver.manage().window().maximize();
        log.info("Maximize window.");
    }

    public static void waitFavoriteLoad(WebDriver driver){
        IndexPage indexPage = initElements(driver, IndexPage.class);
        waitForElementClickable(indexPage.favorites, driver);
    }

    public static Integer parser(String text){
        try {
            return (text!=null && !text.isEmpty()) ? Integer.parseInt(text) : null;
        }
        catch (NumberFormatException e){
            throw new NumberFormatException("NumberFormatException");
        }
    }

    public static boolean elementIsDisplayed(WebElement element, String elementName) {

        try {

            if (element != null && element.isDisplayed()) {
                //Log.info("\"" + elementName + "\" is displayed.");
                return true;
            } else {
                log.info("\"" + elementName + "\" is not displayed.");
                return false;
            }
        } catch (NoSuchElementException e) {
            log.info("\"" + elementName + "\" is NOT displayed.");
            return false;
        } catch (ElementNotVisibleException e) {
            assertTrue(false, "\"" + elementName + "\" was not visible.");
            return false;
        } catch (StaleElementReferenceException e) {
            //ReportService.assertTrue(false, "\"" + elementName + "\" was not in the cache.");
            return false;
        }
    }
    public static void waitPageLoader(String url, int seconds, WebDriver driver) throws InterruptedException {
        try {
            log.info("Waiting for \"" + url + "\" page.");
            int attempt = 0;
            while (!driver.getCurrentUrl().contains(url) && attempt < seconds) {
                attempt++;
                sleep(1);
            }
            log.info("Waiting for \"" + url + "\" page during " + attempt + " seconds.");
            if (!driver.getCurrentUrl().contains(url)) {
                assertTrue(false, "Expected page hasn't loaded  by timeout.\n" +
                        "                                   Current url:" + driver.getCurrentUrl());
            }
        } catch (TimeoutException e) {
            stopLoad(driver);
        }
    }

   /* public static void waitPageLoader(String url, WebDriver driver) {
        waitPageLoader(url, driver);
    }*/

    public static void waitForElementVisible(WebElement element, WebDriver driver) {

        try {
            WebDriverWait wait = new WebDriverWait(driver,20);
            wait.until(ExpectedConditions.visibilityOf(element));
        }
        catch (TimeoutException e){
            assertTrue(false, "ELEMENT: \"" + element + "\" is not presents.");
        }
        catch (StaleElementReferenceException e){
            log.error("ELEMENT: \"" + element + "\" is not found in the cache.");
        }

    }

    public static String getElementText(WebElement element, String elementName) {
        try {
            String text = element.getText();
            log.info("\"" + elementName +"\" content on page  - \"" + text + "\".");
            return text;
        }
        catch (NoSuchElementException | ElementNotVisibleException e){
            assertTrue(false, "\"" + elementName + "\" was not found on page after timeout.");
            throw new NoSuchElementException(e.toString());
        }
    }

    public enum PaymentMethods {
        PayPal,
        Stripe,
        PayProGlobal
    }


}
