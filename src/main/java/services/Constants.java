package services;

/**
 * Created by cosmofish on 30.08.17.
 */
public class Constants {

    public static final String URL = "https://www.templatemonster.com";
    public static final String AUTORIZATION_URL = "https://account.templatemonster.com/auth/#/";
    public static final String TEMPLATE_URL = "https://www.templatemonster.com/joomla-templates/50797.html";



    public static final int PAGE_TIMEOUT = 60;
    public static final int ELEMENT_TIMEOUT = 20;
    public static final int PAGE_ATTEMPT = 30;
}

